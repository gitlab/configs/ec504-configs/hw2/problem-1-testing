package edu.bu.ec504.hw2.p1;


import edu.bu.ec504.hw2.p1.edges.UndirectedEdge;
import edu.bu.ec504.hw2.p1.edges.colored_edges.ColoredDirectedEdge;
import edu.bu.ec504.hw2.p1.edges.weighted_edges.WeightedHyperEdge;
import edu.bu.ec504.hw2.p1.graphs.EdgeListGraph;
import edu.bu.ec504.hw2.p1.graphs.Graph;
import edu.bu.ec504.hw2.p1.support.Color;
import edu.bu.ec504.hw2.p1.vertices.ColoredVertex;
import edu.bu.ec504.hw2.p1.vertices.Vertex;

public class Main {
    // graphs
    static Graph<Vertex, UndirectedEdge<Vertex>> G;
    static Graph<ColoredVertex, ColoredDirectedEdge<ColoredVertex>> H;
    static Graph< ColoredVertex, WeightedHyperEdge<ColoredVertex, String>> I;

    // create graphs

    static void createSimpleGraph() {
        G = new EdgeListGraph<>();

        // vertices
        Vertex Vertex0 = G.addVertex(Vertex.of());
        Vertex Vertex1 = G.addVertex(Vertex.of());
        Vertex Vertex2 = G.addVertex(Vertex.of());

        // edges
        G.addEdge(UndirectedEdge.of(Vertex0, Vertex1));
        G.addEdge(UndirectedEdge.of(Vertex1, Vertex2));
        G.addEdge(UndirectedEdge.of(Vertex2, Vertex0));

        System.out.println("Your simple graph is:\n"+G);

        // check edges
        System.out.println("Edge of " + Vertex0 + " and " + Vertex1 + "? "+G.adjQ(Vertex0, Vertex1));
        System.out.println("Edge of " + Vertex0 + " and " + Vertex2 + "? "+G.adjQ(Vertex0, Vertex2));
        System.out.println("\n\n\n");
    }

    static void createColoredGraph() {
        H = new EdgeListGraph<>();

        // vertices
        ColoredVertex ColoredVertex0 = H.addVertex(ColoredVertex.of(Color.RED));
        ColoredVertex ColoredVertex1 = H.addVertex(ColoredVertex.of(Color.GREEN));
        ColoredVertex ColoredVertex2 = H.addVertex(ColoredVertex.of(Color.BLUE));

        // edges
        H.addEdge(ColoredDirectedEdge.of(ColoredVertex0, ColoredVertex1, Color.YELLOW));
        H.addEdge(ColoredDirectedEdge.of(ColoredVertex0, ColoredVertex0, Color.YELLOW));
        H.addEdge(ColoredDirectedEdge.of(ColoredVertex1, ColoredVertex2, Color.ORANGE));

        System.out.println("Your colored graph is:\n"+H);

        // check edges
        System.out.println("Edge from " + ColoredVertex0 + " to " + ColoredVertex0 + "? "+ H.adjQ(ColoredVertex0, ColoredVertex0));
        System.out.println("Edge from " + ColoredVertex0 + " to " + ColoredVertex1 + "? "+ H.adjQ(ColoredVertex0, ColoredVertex1));
        System.out.println("Edge from " + ColoredVertex0 + " to " + ColoredVertex2 + "? "+ H.adjQ(ColoredVertex0, ColoredVertex2));
        System.out.println("\n\n\n");
    }

    static void createHyperGraph() {
        I = new EdgeListGraph<>();

        // vertices
        ColoredVertex ColoredVertex0 = I.addVertex(ColoredVertex.of(Color.RED));
        ColoredVertex ColoredVertex1 = I.addVertex(ColoredVertex.of(Color.GREEN));
        ColoredVertex ColoredVertex2 = I.addVertex(ColoredVertex.of(Color.BLUE));

        // edges
        I.addEdge(WeightedHyperEdge.of("Nothing"));
        I.addEdge(WeightedHyperEdge.of("One thing",ColoredVertex0));
        I.addEdge(WeightedHyperEdge.of("All things",ColoredVertex0, ColoredVertex1, ColoredVertex2));

        System.out.println("Your hypergraph is:\n"+I);

        // check edges
        System.out.println("Edge with " + ColoredVertex0 + " and " + ColoredVertex0 + "? "+I.adjQ(ColoredVertex0, ColoredVertex0));
        System.out.println("Edge from " + ColoredVertex0 + " to " + ColoredVertex1 + "? "+I.adjQ(ColoredVertex0, ColoredVertex1));
        System.out.println("Edge with " + ColoredVertex0 + " and " + ColoredVertex1 + " and " + ColoredVertex2 + "? "+I.adjQ(ColoredVertex0, ColoredVertex1, ColoredVertex2));
        System.out.println("\n\n\n");
    }

    public static void main(String[] args) {
        createSimpleGraph();
        createColoredGraph();
        createHyperGraph();


    }
}
