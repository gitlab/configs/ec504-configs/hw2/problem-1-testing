package edu.bu.ec504.hw2.p1.graphs;

import edu.bu.ec504.hw2.p1.edges.Edge;
import edu.bu.ec504.hw2.p1.vertices.Vertex;

import java.util.ArrayList;
import java.util.List;

/**
 * Implements an Edge-List representation of a Graph.
 * @param <VertexClass> The class of the Vertices of the graph.
 * @param <EdgeClass> The class of the Edges of the graph.
 */
public class EdgeListGraph <VertexClass extends Vertex, EdgeClass extends Edge<VertexClass>>
        extends Graph<VertexClass,EdgeClass>  {
    // CONSTRUCTOR
    /**
     * Constructs a new EdgeListGraph
     */
    public EdgeListGraph() {
        edgeList = new ArrayList<>();
    }

    // METHODS

    // ... QUERY

    /**
     * @inheritDoc
     */
    @Override
    public boolean adjQ(ArrayList<VertexClass> Vlist) {
        for (EdgeClass edge : edgeList) {
            if (edge.contains(Vlist))
                return true;
        }
        return false;
    }

    /**
     * @return true iff v1 and v2 are adjacent
     */
    public boolean adjQ(VertexClass v1, VertexClass v2) {
        return adjQ(new ArrayList<>(List.of(v1, v2)));
    }

    @Override
    public String toString() {
        final String TAB = "   ";

            StringBuilder result = new StringBuilder();
            result.append(TAB+"Vertices:\n");
            for (VertexClass vert: vertices) {
                result.append(TAB+TAB+vert+"\n");
            }
            result.append("\n"+TAB+"Edges:\n");
            for (EdgeClass edge: edgeList) {
                result.append(TAB+TAB+edge);
                result.append('\n');
            }
            return result.toString();
        }

    // ... MANIPULATION

    @Override
    public VertexClass addVertex(VertexClass v) {
        vertices.add(v);
        return v;
    }

    @Override
    public void addEdge(EdgeClass theEdge) {
        edgeList.add(theEdge);
    }

    // DATA FIELDS

    /** The list of edges for the graph. */
    protected final ArrayList<EdgeClass> edgeList;
}
