package edu.bu.ec504.hw2.p1.support;

/**
 * An enumerated class representing a color.
 */
public enum Color {
  RED,
  ORANGE,
  YELLOW,
  GREEN,
  BLUE,
  INDIGO,
  VIOLET;

  /**
   * @return the next color from this one, cycling back to the first color.
   */
  public Color next() {
    Color[] colors = Color.values();
    return colors[(this.ordinal()+1)%colors.length];
  }

  /**
   * @return The first color in this enum.
   */
  static public Color first() {
    return Color.values()[0];
  }

  @Override
  public String toString() {
    return "the color " + name();
  }
}
