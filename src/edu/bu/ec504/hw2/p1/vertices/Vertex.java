package edu.bu.ec504.hw2.p1.vertices;

import edu.bu.ec504.hw2.p1.support.UID;

/**
 * A generic vertex in a graph.
 * Each Vertex has a unique ID number.
 */

public class Vertex extends UID<Vertex> {

    /**
     * Use the factory method {@link #of()} to create vertices.
     */
    protected Vertex() {
        super();
    }

    /**
     * @return A new Vertex
     */
    public static Vertex of() {
        return new Vertex();
    }

    /**
     * @return If this Vertex has the same ID as <code>otherV</code>
     */
    @Override
    public boolean equals(Object otherV) {
        // self-comparison
        if (this == otherV) return true;

        // null or mismatched classes
        if (otherV == null || getClass() != otherV.getClass()) return false;

        // Cast to the correct type
        Vertex other = (Vertex) otherV;

        // Check vertex IDs
        return myID == other.myID;

    }

    public String toString() { return "Vertex #"+myID; }
}
