package edu.bu.ec504.hw2.p1.edges;

import edu.bu.ec504.hw2.p1.vertices.Vertex;

import java.util.ArrayList;

/**
 * One undirected edge between two vertices.
 */
public class UndirectedEdge<VertexClass extends Vertex> extends Edge<VertexClass> {
    /**
     * Creates an undirected edge between <code>VertexA</code> and <code>VertexB</code>
     *
     * @param theVA One vertex for the edge.
     * @param theVB The other vertex for the edge.
     */
    public UndirectedEdge(VertexClass theVA, VertexClass theVB) {
        myVA = theVA;
        myVB = theVB;
    }

    /**
     * Factory method for producing an undirected edge.
     * @return an undirected graph joining vertex theVA to theVB.
     */
    public static <VC extends Vertex> UndirectedEdge<VC> of(VC theVA, VC theVB) {
        return new UndirectedEdge<>(theVA, theVB);
    }

    // METHODS
    @Override
    public boolean contains(VertexClass V) {
        return V.equals(myVA) || V.equals(myVB);
    }

    /**
     * @return true iff this edge consists of VA and VB.
     */
    public boolean contains(VertexClass VA, VertexClass VB) {
        return contains(VA) && contains(VB);
    }

    /**
     * @param Varr An array of vertices to consider.
     * @return true iff this edge consists of the vertices in <code>Varr</code> .
     */
    @Override
    public boolean contains(ArrayList<VertexClass> Varr) {
        if (Varr.size()!=2)
            return false;
        else return contains(Varr.get(0), Varr.get(1));
    }

    @Override
    public String toString() {
        return "Edge joining [" + myVA + "] and [" + myVB + "] ";
    }

    // FIELDS
    final public VertexClass myVA;
    final public VertexClass myVB;
}
