package edu.bu.ec504.hw2.p1.edges.weighted_edges;

import edu.bu.ec504.hw2.p1.edges.Edge;
import edu.bu.ec504.hw2.p1.vertices.Vertex;

/**
 * Represents a weighted edge.
 * @param <VertexClass> The class of the vertices associated with this edge.
 * @param <WeightClass> The class of the weights associated with this edge.
 */
public abstract class WeightedEdge<VertexClass extends Vertex, WeightClass> extends Edge<VertexClass> {

  /**
   * @return The weight of this edge.
   */
  public WeightClass getWt() {
    return myWt;
  }

  public void setWt(WeightClass wt) {
    this.myWt = wt;
  }

  /**
   * The weight of the edge.
   */
  protected WeightClass myWt;
}
