package edu.bu.ec504.hw2.p1;

import edu.bu.ec504.hw2.p1.edges.UndirectedEdge;
import edu.bu.ec504.hw2.p1.graphs.EdgeListGraph;
import edu.bu.ec504.hw2.p1.graphs.Graph;
import edu.bu.ec504.hw2.p1.support.Color;
import edu.bu.ec504.hw2.p1.vertices.ColoredVertex;

import static edu.bu.ec504.hw2.p1.GraphColorer.color;

public class GraphColorerTester {

    public static void main(String[] args) {
        Graph<ColoredVertex, UndirectedEdge<ColoredVertex>> G = new EdgeListGraph<ColoredVertex, UndirectedEdge<ColoredVertex>>();

        // Build a K_{3,3}
        ColoredVertex[] partA = {
                ColoredVertex.of(Color.first()),
                ColoredVertex.of(Color.first()),
                ColoredVertex.of(Color.first()),
        };
        ColoredVertex[] partB = {
                ColoredVertex.of(Color.first()),
                ColoredVertex.of(Color.first()),
                ColoredVertex.of(Color.first()),
        };

        // build the graph
        // ... vertices
        for (ColoredVertex v : partA) {
            G.addVertex(v);
        }
        for (ColoredVertex v : partB) {
            G.addVertex(v);
        }
        // ... edges
        for (ColoredVertex v : partA)
            for (ColoredVertex w : partB) {
                G.addEdge(UndirectedEdge.of(v,w));
            }

        // color it
        int col = color(G);
        System.out.println("Colored with: "+col+" colors.");
        System.out.println(G);

        if (col!=2) {
            throw new RuntimeException("Coloring is wrong - should be 2 but got "+col);
        }
    }
}
